const matriz = [
    [1, 1, 1, 1],
    [1, 9, 9, 1],
    [1, 9, 9, 1],
    [1, 1, 1, 1]
]

// const rowsSize = matrix.length;
// const colsSize = (matrix || [0])[0].length;

const isBorder = (size, index) => index === 0 || size === index + 1

let accumulator = 0

matrix.forEach((row, rowIndex) => {
    row.forEach((col, colIndex) => {
        accumulator += isBorder(matrix.length, rowIndex) || isBorder(row.length, colIndex) ? col : 0
    })
});


matrix
    .map(r =>
        r.map(c => 
            console.log(c)
        )
    )