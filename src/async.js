const fs = require('fs')

console.log('callback')
fs.readFile('./callback.txt', (err, content) => {
  if(err) return console.log(err)

  console.log(String(content))
})

console.log('Promise')
const readFile = file => new Promise((resolve, reject) => {

  fs.readFile(file, (err, content) => {
    if(err) {
      reject(err)
    } else {
      resolve(content)
    }
  })

})

readFile('./promise.txt')
  .then(content => {
    console.log(String(content))
  })
  .catch(err => {
    console.log(String(err))
  })

console.log('async_await')
const asyncReadFile = async() => {
  const content = await readFile('./async_await.txt')
  console.log(String(content))
}

asyncReadFile()
